﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NXOpen;

namespace Point_Cloud_Creation_From_CAD_Models
{
    public class WorkPoint
    {
        public Body body;
        public Face face;
        public Tag faceTag;
        public Point point;
        public double[] coords = new double[3];
        public Tag tag;
        public string nonDecomLabel;
        public string bodyLabel;
        public string faceLabel;
        public double[] normal;
        public string adjFaceLabel;

        public WorkPoint(Point point)
        {
            this.point = point;
            this.tag = point.Tag;
            this.coords[0] = point.Coordinates.X;
            this.coords[1] = point.Coordinates.Y;
            this.coords[2] = point.Coordinates.Z;
        }
    }
}
