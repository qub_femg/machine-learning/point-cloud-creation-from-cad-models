﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using NXOpen;
using NXOpen.UF;
using KDTreeDLL;

namespace Point_Cloud_Creation_From_CAD_Models
{
    /// <summary>
    /// Class of operations to retrieve labels for a point cloud.
    /// </summary>
    public static class PointCloudOps
    {
        ///<summary>
        ///This was used to find the appropiate tolerance for the bounding boxes.
        /// </summary>
        public static void ToleranceSelection()
        {
            int totalFaces = 0;
            List<double> faceDist = new List<double>();
            double avgDist;
            double maxDist;

            foreach (Body b in NXVariables.workPart.Bodies)
            {
                totalFaces += b.GetFaces().Length;
            }

            foreach (WorkPoint wp in NXVariables.workPoints.Values)
            {
                int faceCount = 0;
                double[] faceArray = new double[totalFaces];

                foreach (Body b in NXVariables.workPart.Bodies)
                {
                    foreach (Face f in b.GetFaces())
                    {
                        double[] blank = new double[3];
                        double minDist;
                        double[] ptObj1 = new double[3];
                        double[] ptObj2 = new double[3];

                        NXVariables.ufs.Modl.AskMinimumDist(wp.tag, f.Tag, 1, wp.coords, 0, blank, out minDist, ptObj1, ptObj2);

                        faceArray[faceCount] = minDist;
                        faceCount++;
                    }
                }
                faceDist.Add(faceArray.Min());
            }

            avgDist = faceDist.Sum() / NXVariables.workPoints.Count();
            NXVariables.avgDists.Add(avgDist);
            NXVariables.lw.WriteFullline("Avg Dist: " + avgDist.ToString());

            maxDist = faceDist.Max();
            NXVariables.maxDists.Add(maxDist);
            NXVariables.lw.WriteFullline("Max Dist: " + maxDist.ToString());

            string csvPath = UserVariables.dirPath + "tolerance.csv";
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@csvPath, true))
            {
                file.WriteLine(UserVariables.baseName + ", " + avgDist.ToString() + ", " + maxDist.ToString() + ", " + totalFaces.ToString());
            }
        }

        public static void NewSampledPoints()
        {
            List<WorkFace> noPointsFaces = new List<WorkFace>();
            var random = new Random();

            FindInternalFaces();
            BoundingBoxOp();

            var externalFaces = from face in NXVariables.workFaces.Values
                                where face.faceType is "External"
                                select face;

            foreach (WorkFace face in externalFaces)
            {
                face.areaToPointRatio = face.workPoints.Count() / face.GetArea();
                NXVariables.workFaces[face.tag].areaToPointRatio = face.areaToPointRatio;
                NXVariables.ratios.Add(face.areaToPointRatio);

                if (face.areaToPointRatio == 0)
                {
                    noPointsFaces.Add(face);
                    //face.face.Color = 186;
                    //face.face.RedisplayObject();
                }
            }

            if (noPointsFaces.Count() != 0)
            {
                foreach (WorkFace face in noPointsFaces)
                {
                    AddPointToFace(face, UserVariables.minNumPoints);
                }
            }
            noPointsFaces.Clear();
            noPointsFaces.TrimExcess();
            externalFaces = new List<WorkFace>();
        }

        ///<summary>
        ///This will find the internal faces in the model and calculate the ratio of num
        ///of points to area for the external faces. If this is zero, points will be transfered
        ///from faces with high ratios to those with a ratio of 0.
        /// </summary>
        public static void PointRelocation()
        {
            List<WorkFace> noPointsFaces = new List<WorkFace>();
            var random = new Random();

            FindInternalFaces();
            BoundingBoxOp();

            var externalFaces = from face in NXVariables.workFaces.Values
                                where face.faceType is "External"
                                select face;

            foreach (WorkFace face in externalFaces)
            {
                face.areaToPointRatio = face.workPoints.Count() / face.GetArea();
                NXVariables.workFaces[face.tag].areaToPointRatio = face.areaToPointRatio;
                NXVariables.ratios.Add(face.areaToPointRatio);

                if (face.areaToPointRatio == 0)
                {
                    noPointsFaces.Add(face);
                }
            }

            if (noPointsFaces.Count() != 0)
            {
                var aboveAvg = from face in externalFaces
                               orderby face.areaToPointRatio
                               where face.areaToPointRatio > NXVariables.ratios.Average()
                               select face;

                foreach (WorkFace face in noPointsFaces)
                {
                    int numOfPoints = Convert.ToInt32(NXVariables.ratios.Average() * face.GetArea());

                    if (numOfPoints < UserVariables.minNumPoints)
                    {
                        numOfPoints = UserVariables.minNumPoints;
                    }

                    int i = 0;
                    int j = 0;

                    while (i < numOfPoints)
                    {
                        j++;
                        if (j > 50) break;

                        try
                        {
                            int randomIndex = random.Next(0, aboveAvg.Count());
                            NXVariables.workFaces[aboveAvg.ElementAt(randomIndex).tag].workPoints.RemoveAt(0);
                            AddPointToFace(face, numOfPoints);
                            i++;
                        }
                        catch (Exception e)
                        {
                            if (UserVariables.useListingWindow) NXVariables.lw.WriteFullline(e.ToString());
                            continue;
                        }
                    }

                }
                aboveAvg = new List<WorkFace>();
            }
            noPointsFaces.Clear();
            noPointsFaces.TrimExcess();
            externalFaces = new List<WorkFace>();
        }

        ///<summary>
        ///Finds the internal faces in the part by creating a kd tree of the centroids of the solid bodies.
        ///For centroids near those of sheet bodies, if they also have similar areas are considered internal
        ///faces.
        /// </summary>
        public static void FindInternalFaces()
        {
            var solidFaces = from face in NXVariables.workFaces.Values
                             where face.bodyType is "Solid"
                             select face;

            var sheetFaces = from face in NXVariables.workFaces.Values
                             where face.bodyType is "Sheet"
                             select face;

            CreateCentroidKDTree(solidFaces);

            foreach (WorkFace f in sheetFaces)
            {
                List<int> ids = NearestPoints(f.centroid, 2);

                foreach (int id in ids)
                {
                    if ((NXVariables.workFaces[(Tag)id].GetArea() > f.GetArea() - 0.001)
                        && (NXVariables.workFaces[(Tag)id].GetArea() < f.GetArea() + 0.001))
                    {
                        //NXVariables.workFaces[(Tag)id].face.Color = 186;
                        //NXVariables.workFaces[(Tag)id].face.RedisplayObject();
                        NXVariables.workFaces[(Tag)id].SetFaceType("Internal");
                    }
                }
            }
            NXVariables.workFaces.Values.Where(c => c.bodyType is "Solid")
                .Where(d => string.IsNullOrEmpty(d.faceType)).ToList().ForEach(cc => cc.faceType = "External");
        }

        public static class TheKDtree
        {
            public static KDTree kdTree;
        }

        private static void CreateCentroidKDTree(IEnumerable<WorkFace> faces)
        {
            KDTree kd = new KDTree(3);

            foreach (WorkFace face in faces)
            {
                try { kd.insert(face.centroid, (int)face.tag); }
                catch (Exception ex)
                {
                    if (UserVariables.useListingWindow) NXVariables.lw.WriteFullline(ex.ToString());
                    face.centroid[0] += 0.00000001;
                    kd.insert(face.centroid, (int)face.tag);
                }
            }
            TheKDtree.kdTree = kd;
        }

        public static List<int> NearestPoints(double[] point, int count)
        {

            Object[] x = TheKDtree.kdTree.nearest(point, count);
            List<int> ids = x.Select(a => (int)a).ToList();
            return ids;
        }

        public static void BoundingBoxOp()
        {
            bool terminate = false;

            foreach (WorkPoint wp in NXVariables.workPoints.Values)
            {
                var filteredFaces = from face in NXVariables.workFaces.Values
                                        //where face.faceType == "External"
                                    where face.bBox.xL <= wp.coords[0]
                                    where face.bBox.xH >= wp.coords[0]
                                    where face.bBox.yL <= wp.coords[1]
                                    where face.bBox.yH >= wp.coords[1]
                                    where face.bBox.zL <= wp.coords[2]
                                    where face.bBox.zH >= wp.coords[2]
                                    select face.tag;

                double[] faceArray = new double[filteredFaces.Count()];
                Tag[] faceTag = new Tag[filteredFaces.Count()];
                int faceCount = 0;

                foreach (Tag f in filteredFaces)
                {
                    double[] blank = new double[3];
                    double minDist;
                    double[] ptObj1 = new double[3];
                    double[] ptObj2 = new double[3];

                    NXVariables.ufs.Modl.AskMinimumDist(wp.tag, f, 1, wp.coords, 0, blank, out minDist, ptObj1, ptObj2);

                    faceArray[faceCount] = minDist;
                    faceTag[faceCount] = f;

                    if (minDist == 0)
                    {
                        terminate = true;
                        break;
                    }

                    faceCount++;
                }

                int minIndex;

                if (terminate)
                {
                    minIndex = faceCount;
                    terminate = false;
                }
                else
                {
                    minIndex = Array.IndexOf(faceArray, faceArray.Min());
                }

                NXVariables.workFaces[faceTag[minIndex]].workPoints.Add(wp.tag);
                NXVariables.workPoints[wp.tag].faceTag = faceTag[minIndex];
                NXVariables.workPoints[wp.tag].faceLabel = NXVariables.workFaces[faceTag[minIndex]].faceLabel;
            }
        }

        private static void AddPointToFace(WorkFace face, int numOfPoints)
        {
            double[] uvMinMax = new double[4];

            NXVariables.ufs.Modl.AskFaceUvMinmax(face.tag, uvMinMax);

            int numPtsU = numOfPoints / 2;
            int numPtsV = numOfPoints / 2;

            double stepU = (uvMinMax[1] - uvMinMax[0]) / numPtsU;
            double stepV = (uvMinMax[3] - uvMinMax[2]) / numPtsV;

            for (int i = 0; i < numPtsU; i++)
            {
                for (int j = 0; j < numPtsV; j++)
                {
                    double[] param = new double[2];
                    param[0] = uvMinMax[0] + i * stepU;
                    param[1] = uvMinMax[2] + j * stepV;

                    double[] pt = new double[3];
                    double[] u1 = new double[3];
                    double[] v1 = new double[3];
                    double[] u2 = new double[3];
                    double[] v2 = new double[3];
                    double[] norm = new double[3];
                    double[] radii = new double[2];

                    NXVariables.ufs.Modl.AskFaceProps(face.tag, param, pt, u1, v1, u2, v2, norm, radii);


                    Point3d point3D;
                    point3D.X = pt[0];
                    point3D.Y = pt[1];
                    point3D.Z = pt[2];

                    Point p = NXVariables.workPart.Points.CreatePoint(point3D);
                    p.SetVisibility(SmartObject.VisibilityOption.Visible);
                    //p.SetName(face.tag.ToString());
                    p.RedisplayObject();

                    WorkPoint wp = new WorkPoint(p);
                    wp.body = face.body;
                    wp.face = face.face;
                    wp.faceTag = face.tag;
                    wp.normal = norm;
                    wp.bodyLabel = face.bodyLabel;

                    NXVariables.workFaces[face.tag].workPoints.Add(wp.tag);
                    NXVariables.workPoints.Add(wp.tag, wp);
                }
            }
        }

        private static void DeleteObject(int objectTag)
        {
            NXOpen.Session.UndoMarkId markId8;
            markId8 = NXVariables.s.SetUndoMark(NXOpen.Session.MarkVisibility.Invisible, "Delete");
            NXObject[] objects = new NXObject[1];
            objects[0] = (NXObject)NXOpen.Utilities.NXObjectManager.Get((Tag)objectTag);
            int nErrs1;
            nErrs1 = NXVariables.s.UpdateManager.AddToDeleteList(objects);
            bool notifyOnDelete2;
            notifyOnDelete2 = NXVariables.s.Preferences.Modeling.NotifyOnDelete;
            NXVariables.s.UpdateManager.ClearErrorList();
            NXVariables.s.UpdateManager.DoUpdate(markId8);
        }

        public static void SearchSpaceExperiment()
        {
            foreach (WorkPoint wp in NXVariables.workPoints.Values)
            {
                FaceSearchSpaceTest(wp);
            }
        }

        private static void FaceSearchSpaceTest(WorkPoint wp)
        {
            var filteredFaces = from face in NXVariables.workFaces.Values
                                where face.bBox.xL <= wp.coords[0]
                                where face.bBox.xH >= wp.coords[0]
                                where face.bBox.yL <= wp.coords[1]
                                where face.bBox.yH >= wp.coords[1]
                                where face.bBox.zL <= wp.coords[2]
                                where face.bBox.zH >= wp.coords[2]
                                select face.tag;

            NXVariables.faceSearchSpace.Add(filteredFaces.Count());
        }

        // Only use on undecomposed models with one body
        public static void FindAdjacentFace()
        {
            IEnumerable<Tag> edges = new List<Tag>();
            var potentialFaces = from wp in NXVariables.workPoints.Values
                                 select wp.faceTag;

            potentialFaces = potentialFaces.Distinct();

            foreach (Tag f in potentialFaces)
            {
                NXVariables.ufs.Modl.AskAdjacFaces(f, out Tag[] adjFaces);
                NXVariables.workFaces[f].adjFaces = adjFaces;
            }

            foreach (WorkPoint wp in NXVariables.workPoints.Values)
            {
                Dictionary<Tag, double> minDists = new Dictionary<Tag, double>();
                foreach (Tag adjFace in NXVariables.workFaces[wp.faceTag].adjFaces)
                {
                    double[] blank = new double[3];
                    double minDist;
                    double[] ptObj1 = new double[3];
                    double[] ptObj2 = new double[3];

                    NXVariables.ufs.Modl.AskMinimumDist(wp.tag, adjFace, 1, wp.coords, 0, blank, out minDist, ptObj1, ptObj2);
                    minDists.Add(adjFace, minDist);
                }
                if (minDists.Count() != 0)
                {
                    Tag faceTag = minDists.OrderBy(kvp => kvp.Value).First().Key;
                    wp.point.SetName(NXVariables.workFaces[faceTag].face.Name);
                    wp.adjFaceLabel = NXVariables.workFaces[faceTag].face.Name;
                }
            }
        }

        /// <summary>
        /// Get true face labels from CSV.
        /// </summary>
        public static void GetLabelsFromFaceTruthCSV()
        {
            foreach (WorkFace wf in NXVariables.workFaces.Values)
            {
                int idx = Convert.ToInt32(wf.face.Name);
                wf.faceLabel = UserVariables.faceTruthDict[UserVariables.baseName][idx];
            }
        }
    }
}


