﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Point_Cloud_Creation_From_CAD_Models
{
    public class BoundingBox
    {
        public int faceTag;
        public double xL;
        public double xH;
        public double yL;
        public double yH;
        public double zL;
        public double zH;
    }
}

