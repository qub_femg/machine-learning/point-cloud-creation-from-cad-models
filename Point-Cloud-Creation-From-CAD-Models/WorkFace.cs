﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NXOpen;

namespace Point_Cloud_Creation_From_CAD_Models
{
    public class WorkFace
    {
        public Body body;
        public Face face;
        public Tag tag;
        public BoundingBox bBox;
        public string bodyType;
        public string bodyLabel = null;
        public string faceType = null;
        public string faceLabel = null;
        private double area;
        public double[] centroid;
        public List<Tag> workPoints = new List<Tag>();
        public double areaToPointRatio;
        public List<Tag> edgeTags = new List<Tag>();
        public Tag[] adjFaces;

        public WorkFace(Body body, Face face)
        {
            this.body = body;
            this.face = face;
            this.tag = this.face.Tag;

            if (this.body.IsSolidBody)
            {
                this.bodyType = "Solid";
            }
            else if (this.body.IsSheetBody)
            {
                this.bodyType = "Sheet";
                this.SetFaceType("Sheet");
            }
            else
            {
                this.bodyType = "Convergent";
                this.SetFaceType("Convergent");
            }
        }

        public double GetArea()
        {
            return this.area;
        }

        public void SetFaceType(string faceType)
        {
            this.faceType = faceType;
        }

        public void SetAreaCentroid()
        {
            Tag sourceFaceSheetTag;
            NXVariables.ufs.Modl.ExtractFace(this.tag, 0, out sourceFaceSheetTag);
            Tag[] sheetTagArray = { sourceFaceSheetTag };
            double[] accuracy = new double[11];
            accuracy[0] = 1.0;
            double[] massProps = new double[47];
            double[] statistics = new double[13];
            NXVariables.ufs.Modl.AskMassProps3d(sheetTagArray, 1, 2, 4, 1, 2, accuracy, massProps, statistics);
            double[] centroid = { massProps[3] * 1000, massProps[4] * 1000, massProps[5] * 1000 };
            this.centroid = centroid;
            this.area = massProps[0] * 1000000;
            DeleteObject((int)sourceFaceSheetTag);
        }

        public void SetBBox(double tol)
        {
            BoundingBox bBox = new BoundingBox();
            double[] bBox_coords = new double[6];
            NXVariables.ufs.Modl.AskBoundingBox(this.tag, bBox_coords);

            bBox.xL = bBox_coords[0] - tol;
            bBox.xH = bBox_coords[3] + tol;
            bBox.yL = bBox_coords[1] - tol;
            bBox.yH = bBox_coords[4] + tol;
            bBox.zL = bBox_coords[2] - tol;
            bBox.zH = bBox_coords[5] + tol;

            this.bBox = bBox;
        }

        private void DeleteObject(int objectTag)
        {
            NXOpen.Session.UndoMarkId markId8;
            markId8 = NXVariables.s.SetUndoMark(NXOpen.Session.MarkVisibility.Invisible, "Delete");
            NXObject[] objects = new NXObject[1];
            objects[0] = (NXObject)NXOpen.Utilities.NXObjectManager.Get((Tag)objectTag);
            int nErrs;
            nErrs = NXVariables.s.UpdateManager.AddToDeleteList(objects);
            bool notifyOnDelete;
            notifyOnDelete = NXVariables.s.Preferences.Modeling.NotifyOnDelete;
            NXVariables.s.UpdateManager.ClearErrorList();
            NXVariables.s.UpdateManager.DoUpdate(markId8);
        }

        public void SetEdgeTags()
        {
            foreach (Edge e in this.face.GetEdges())
            {
                this.edgeTags.Add(e.Tag);
            }
        }
    }
}

