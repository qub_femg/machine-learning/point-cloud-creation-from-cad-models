﻿using System;
using System.IO;
using System.Collections.Generic;

namespace Point_Cloud_Creation_From_CAD_Models
{
    static class UserVariables
    {
        public static string methodType;
        public static bool useListingWindow;
        public static bool saveWorkParts;
        public static bool isDecomposition;
        public static string fileExtension;

        // File properties
        public static string dirPath;
        public static string baseName;
        public static string subDirPath;
        public static string nonDecomposedPath;
        public static string decomposedPath;
        public static string nonDecomSavePath;
        public static string pointPartSavePath;
        public static string originalPartPath;
        public static string oldLabelledPCPath;

        // stl Properties
        public static string stlPath;
        public static double stlTriangleTol = 0.008;
        public static double stlAdjacencyTol = 0.008;

        // Point cloud properties
        public static string cloudComparePath;
        public static string batchScriptName;
        public static int pointCloudSize;
        public static string unlabelledPCPath;
        public static string labelledPCPath;
        public static double boundingBoxTol = 2.6;

        // KD Tree
        public static double kdTolerance = 0.1;

        // Point Relocation
        public static int minNumPoints = 4;

        public static Dictionary<string, List<string>> faceTruthDict = new Dictionary<string, List<string>>(); //key->FileName, value->FeatureCode
    }
}


