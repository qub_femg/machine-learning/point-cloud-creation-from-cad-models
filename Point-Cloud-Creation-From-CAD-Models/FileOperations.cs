﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

using NXOpen;

namespace Point_Cloud_Creation_From_CAD_Models
{
    public static class FileOperations
    {
        /// <summary>
        /// Export current workpart to STL format.
        /// </summary>
        public static void ExportSTL()
        {
            STLCreator sTLCreator;
            sTLCreator = NXVariables.s.DexManager.CreateStlCreator();
            sTLCreator.AutoNormalGen = true;
            sTLCreator.ErrorMessageDisplay = false;
            sTLCreator.TriangleTol = UserVariables.stlTriangleTol;
            sTLCreator.AdjacencyTol = UserVariables.stlAdjacencyTol;
            sTLCreator.OutputFile = UserVariables.stlPath;
            sTLCreator.NormalDisplay = false;
            sTLCreator.TriangleDisplay = false;

            foreach (Body b in NXVariables.bodies)
            {
                sTLCreator.ExportSelectionBlock.Add(b);
            }

            sTLCreator.Commit();
            sTLCreator.Destroy();
        }

        /// <summary>
        /// Used to convert STL to point cloud.
        /// Run Cloud Compare software from batch script.
        /// Batch script must be saved in program install location.
        /// </summary>
        /// <param name="pointCloudPath">Path to where point cloud will be saved.</param>
        public static void CreatePointCloud(string pointCloudPath)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.WorkingDirectory = UserVariables.cloudComparePath;
            startInfo.Arguments = string.Format("/C {0} {1} {2} {3}", UserVariables.batchScriptName, UserVariables.stlPath, UserVariables.pointCloudSize, pointCloudPath);
            process.StartInfo = startInfo;
            process.Start();
            process.WaitForExit();
        }

        /// <summary>
        /// Import point cloud to NX.
        /// </summary>
        /// <param name="pointCloudPath">Path to point cloud txt file</param>
        public static void LoadPointCloud(string pointCloudPath)
        {
            NXOpen.GeometricUtilities.PointsFromFileBuilder pointsFromFileBuilder;
            pointsFromFileBuilder = NXVariables.workPart.CreatePointsFromFileBuilder();
            pointsFromFileBuilder.FileName = pointCloudPath;
            pointsFromFileBuilder.CoordinateOption = NXOpen.GeometricUtilities.PointsFromFileBuilder.Options.Absolute;
            NXObject pointCloud = pointsFromFileBuilder.Commit();
            pointsFromFileBuilder.Destroy();

            PointCollection pointCollection = pointCloud.OwningPart.Points;
            List<Point> pointCloudList = pointCollection.ToArray().OfType<Point>().ToList();

            string[] lines = System.IO.File.ReadAllLines(pointCloudPath);

            int count = 0;

            foreach (Point p in pointCloudList)
            {
                string[] parameters = lines[count].Split(' ');

                WorkPoint wp = new WorkPoint(p);
                wp.normal = new double[] { Convert.ToDouble(parameters[3]), Convert.ToDouble(parameters[4]), Convert.ToDouble(parameters[5]) };
                NXVariables.workPoints.Add(p.Tag, wp);

                count++;
            }
            lines = null;
        }

        public static void WriteLabelledPointCloudFile(string currentPointCloudPath, string newPointCloudPath,
            bool normal = true, bool faceTag = true, bool bodyLabel = true, bool adjFaceLabel = false, bool faceLabel = false)
        {
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@newPointCloudPath, true))
            {
                foreach (WorkPoint point in NXVariables.workPoints.Values)
                {
                    string line = String.Format("{0} {1} {2}", point.coords[0], point.coords[1], point.coords[2]);

                    if (normal)
                    {
                        line += " " + String.Format("{0} {1} {2}", point.normal[0], point.normal[1], point.normal[2]);
                    }

                    if (faceTag)
                    {
                        line += " " + point.faceTag.ToString();
                    }

                    if (bodyLabel)
                    {
                        line += " " + point.bodyLabel;
                    }

                    if (adjFaceLabel)
                    {
                        line += " " + point.adjFaceLabel;
                    }

                    if (faceLabel)
                    {
                        line += " " + point.faceLabel;
                    }

                    file.WriteLine(line);
                }
            }
        }

        public static void AddAdjFaceToCloudFile(string currentPointCloudPath, string newPointCloudPath)
        {
            String path = currentPointCloudPath;
            List<String> lines = new List<String>();

            if (File.Exists(path))
            {
                using (StreamReader reader = new StreamReader(path))
                {
                    String line;
                    int count = 0;

                    while ((line = reader.ReadLine()) != null)
                    {
                        WorkPoint wp = NXVariables.workPoints.ElementAt(1).Value;
                        line += " " + wp.adjFaceLabel;

                        count++;
                        lines.Add(line);
                    }
                }

                using (StreamWriter writer = new StreamWriter(newPointCloudPath, false))
                {
                    foreach (String line in lines)
                        writer.WriteLine(line);
                }
            }
        }

        public static void ReadFaceTruthFile(string txtPath)
        {
            using (var reader = new StreamReader(txtPath))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');

                    UserVariables.faceTruthDict.Add(values[0], values.Skip(1).ToList());
                }
            }
        }
    }
}