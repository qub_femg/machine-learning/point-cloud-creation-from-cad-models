﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

using NXOpen;
using NXOpen.UF;

namespace Point_Cloud_Creation_From_CAD_Models
{
    public static class Programs
    {
        public static void ProcessDataset()
        {
            InitialiseNXVariables();

            string labelPath = String.Format("{0}labels.csv", UserVariables.dirPath);
            FileOperations.ReadFaceTruthFile(labelPath);

            foreach (string inputFile in Directory.GetFiles(UserVariables.dirPath, UserVariables.fileExtension))
            {
                SetFilePaths(inputFile);
                ProcessPart();
                ResetNXVariables();
            }

            //string[] directories = Directory.GetDirectories(UserVariables.dirPath);

            /*
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(UserVariables.dirPath + "new_points_10000.csv", true))
            {
                file.WriteLine("Part" + "," + "Time (ms)");
            }
            
            foreach (string subDir in directories)
            {
                //var timer = System.Diagnostics.Stopwatch.StartNew();
                UserVariables.subDirPath = subDir;
                foreach (string inputFile in Directory.GetFiles(subDir, UserVariables.fileExtension))
                { 
                    SetFilePaths(inputFile);
                    ProcessPart();
                    ResetNXVariables();
                }
                //timer.Stop();
                
                
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(UserVariables.dirPath + "new_points_10000.csv", true))
                {
                    file.WriteLine(UserVariables.baseName + "," + timer.ElapsedMilliseconds.ToString());
                }
                
                NXVariables.ratios.Clear();
                NXVariables.ratios = new List<double>();
            }
            */
            if (UserVariables.useListingWindow)
            {
                NXVariables.lw.Close();
                NXVariables.lw.SelectDevice(ListingWindow.DeviceType.Window, "");
            }
        }

        private static void ProcessPart()
        {
            try
            {
                LoadNewPart(UserVariables.nonDecomposedPath);
                NXVariables.bodies = NXVariables.workPart.Bodies.ToArray();
                FileOperations.ExportSTL();
                FileOperations.CreatePointCloud(UserVariables.unlabelledPCPath);

                if (UserVariables.saveWorkParts) NXVariables.workPart.SaveAs(UserVariables.nonDecomSavePath);

                if (UserVariables.isDecomposition)
                {
                    ClosePart();
                    LoadNewPart(UserVariables.decomposedPath);
                    NXVariables.bodies = NXVariables.workPart.Bodies.ToArray();
                    if (UserVariables.saveWorkParts) NXVariables.workPart.SaveAs(UserVariables.decomposedPath);
                }

                FileOperations.LoadPointCloud(UserVariables.unlabelledPCPath);

                LoadWorkFaces();
                PointCloudOps.GetLabelsFromFaceTruthCSV();

                if (UserVariables.methodType is "basic")
                {
                    //PointCloudOps.FindInternalFaces();
                    PointCloudOps.BoundingBoxOp();
                }
                else if (UserVariables.methodType is "relocation")
                {
                    PointCloudOps.PointRelocation();
                }
                else if (UserVariables.methodType is "new_points")
                {
                    PointCloudOps.NewSampledPoints();
                }

                FileOperations.WriteLabelledPointCloudFile(UserVariables.unlabelledPCPath, UserVariables.labelledPCPath, true, true, false, false, true);

                if (UserVariables.saveWorkParts) NXVariables.workPart.SaveAs(UserVariables.pointPartSavePath);
            }

            catch (NXException nxe)
            {
                if (UserVariables.useListingWindow) NXVariables.lw.WriteFullline(nxe.ToString());
                return;
            }

            catch (Exception e)
            {
                if (UserVariables.useListingWindow) NXVariables.lw.WriteFullline(e.ToString());
                return;
            }

            finally
            {
                ClosePart();
            }
        }

        /// <summary>
        /// This method initialises the nx variables that willbe stored in the NXVariables class.
        /// </summary>
        /// <param name="listingWindow"></param>
        private static void InitialiseNXVariables()
        {
            NXVariables.s = Session.GetSession();
            NXVariables.ufs = UFSession.GetUFSession();
            NXVariables.partCleanup = NXVariables.s.NewPartCleanup();
            NXVariables.ufs.Abort.EnableAbort();

            if (UserVariables.useListingWindow)
            {
                NXVariables.lw = NXVariables.s.ListingWindow;
                NXVariables.lw.SelectDevice(ListingWindow.DeviceType.Window, "");
                NXVariables.lw.Open();
            }
        }

        /// <summary>
        /// This method is used to reset variables in the NXVariables class to their default.
        /// </summary>
        private static void ResetNXVariables()
        {
            NXVariables.workFaces.Clear();
            NXVariables.workPoints.Clear();
            NXVariables.workFaces = new Dictionary<Tag, WorkFace>();
            NXVariables.workPoints = new Dictionary<Tag, WorkPoint>();
            NXVariables.bodies = null;
        }

        /// <summary>
        /// This method will set the file paths needed for the majority of operations using the
        /// dirpath variable set in the SetInitUserVariables() method in MainProgram class.
        /// </summary>
        /// <param name="inputFilePath"></param>
        private static void SetFilePaths(string inputFilePath = null)
        {
            UserVariables.nonDecomposedPath = inputFilePath;
            UserVariables.baseName = UserVariables.nonDecomposedPath.Substring(UserVariables.dirPath.Length, UserVariables.nonDecomposedPath.Length - UserVariables.dirPath.Length - UserVariables.fileExtension.Length + 1);
            //UserVariables.baseName = UserVariables.subDirPath.Substring(UserVariables.dirPath.Length, UserVariables.subDirPath.Length - UserVariables.dirPath.Length);
            UserVariables.subDirPath = UserVariables.dirPath;

            string unlabelledDir = String.Format("{0}//Unlabelled_PCs", UserVariables.dirPath);
            if (!(Directory.Exists(unlabelledDir)))
            {
                Directory.CreateDirectory(unlabelledDir);
            }

            string labelledDir = String.Format("{0}//Labelled_PCs", UserVariables.dirPath);
            if (!(Directory.Exists(labelledDir)))
            {
                Directory.CreateDirectory(labelledDir);
            }

            string stlDir = String.Format("{0}//STLs", UserVariables.dirPath);
            if (!(Directory.Exists(stlDir)))
            {
                Directory.CreateDirectory(stlDir);
            }

            UserVariables.originalPartPath = String.Format("{0}//{1}_orig.prt", UserVariables.subDirPath, UserVariables.baseName);
            UserVariables.decomposedPath = String.Format("{0}//{1}.prt", UserVariables.subDirPath, UserVariables.baseName);
            UserVariables.stlPath = String.Format("{0}//{1}//{2}.stl", UserVariables.subDirPath, "STLs", UserVariables.baseName);
            UserVariables.unlabelledPCPath = String.Format("{0}//{1}//{2}_unlabelled.txt", UserVariables.subDirPath, "Unlabelled_PCs", UserVariables.baseName);
            UserVariables.oldLabelledPCPath = String.Format("{0}//{1}_labelled.txt", UserVariables.subDirPath, UserVariables.baseName);
            UserVariables.nonDecomSavePath = String.Format("{0}//{1}_orig.prt", UserVariables.subDirPath, UserVariables.baseName);
            UserVariables.pointPartSavePath = String.Format("{0}//{1}_{2}.prt", UserVariables.subDirPath, UserVariables.baseName, UserVariables.pointCloudSize);

            string decompType = "non_decomp";
            if (UserVariables.isDecomposition) decompType = "decomp";
            UserVariables.labelledPCPath = String.Format("{0}//{1}//{2}_{3}_{4}_{5}.txt", UserVariables.subDirPath, "Labelled_PCs", UserVariables.baseName, UserVariables.methodType, decompType, UserVariables.pointCloudSize);
        }

        private static void LoadNewPart(string filePath)
        {
            PartLoadStatus loadStatus;
            NXVariables.workPart = NXVariables.s.Parts.OpenDisplay(filePath, out loadStatus);
            loadStatus.Dispose();
        }

        private static void ClosePart()
        {
            NXVariables.workPart.Close(BasePart.CloseWholeTree.True, BasePart.CloseModified.CloseModified, null);
            NXVariables.partCleanup.DoCleanup();
            NXVariables.partCleanup.Reset();
        }

        private static void LoadWorkFaces()
        {
            foreach (Body body in NXVariables.bodies)
            {
                foreach (Face face in body.GetFaces())
                {
                    WorkFace workFace = new WorkFace(body, face);
                    workFace.SetAreaCentroid();
                    workFace.SetBBox(UserVariables.boundingBoxTol);
                    workFace.bodyLabel = body.Name;
                    workFace.SetEdgeTags();

                    NXVariables.workFaces.Add(workFace.tag, workFace);
                }
            }
        }

        public static void ToleranceCal(bool listingWindow = true)
        {
            double maxDist;
            InitialiseNXVariables();

            string[] directories = Directory.GetDirectories(UserVariables.dirPath);

            foreach (string subDir in directories)
            {
                UserVariables.subDirPath = subDir;
                foreach (string inputFile in Directory.GetFiles(subDir, "*.x_t"))
                {
                    SetFilePaths(inputFile);

                    try
                    {
                        LoadNewPart(UserVariables.pointPartSavePath);

                        NXVariables.bodies = NXVariables.workPart.Bodies.ToArray();
                        LoadWorkFaces();

                        Point[] points = NXVariables.workPart.Points.ToArray();

                        foreach (Point p in points)
                        {
                            WorkPoint wp = new WorkPoint(p);
                            NXVariables.workPoints.Add(p.Tag, wp);
                        }

                        PointCloudOps.ToleranceSelection();

                    }
                    catch (NXException e)
                    {
                        if (UserVariables.useListingWindow) NXVariables.lw.WriteFullline(e.ToString());
                        return;
                    }
                    catch (Exception e)
                    {
                        if (UserVariables.useListingWindow) NXVariables.lw.WriteFullline(e.ToString());
                        return;
                    }
                    finally
                    {
                        ClosePart();
                    }

                    ResetNXVariables();
                }
            }


            double avg = NXVariables.avgDists.Sum() / NXVariables.avgDists.Count;
            maxDist = NXVariables.maxDists.Max();

            NXVariables.lw.WriteFullline("Avg: " + avg.ToString());
            NXVariables.lw.WriteFullline("Max: " + maxDist.ToString());

            string csvPath = UserVariables.dirPath + "tolerance.csv";
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@csvPath, true))
            {
                file.WriteLine("Total, " + avg.ToString() + ", " + maxDist.ToString());
            }

            if (listingWindow)
            {
                NXVariables.lw.Close();
                NXVariables.lw.SelectDevice(ListingWindow.DeviceType.Window, "");
            }
        }

        public static void SearchSpaceExperiment()
        {
            InitialiseNXVariables();

            string[] directories = Directory.GetDirectories(UserVariables.dirPath);

            foreach (string subDir in directories)
            {
                UserVariables.subDirPath = subDir;
                foreach (string inputFile in Directory.GetFiles(subDir, "*.x_t"))
                {
                    SetFilePaths(inputFile);

                    try
                    {
                        LoadNewPart(UserVariables.pointPartSavePath);
                        NXVariables.bodies = NXVariables.workPart.Bodies.ToArray();
                        LoadWorkFaces();

                        Point[] points = NXVariables.workPart.Points.ToArray();

                        foreach (Point p in points)
                        {
                            WorkPoint wp = new WorkPoint(p);
                            NXVariables.workPoints.Add(p.Tag, wp);
                        }

                        PointCloudOps.SearchSpaceExperiment();
                        NXVariables.avgFaceSearchSpace.Add(NXVariables.faceSearchSpace.Average());
                        NXVariables.faceSearchSpace.Clear();
                    }
                    catch (NXException e)
                    {
                        if (UserVariables.useListingWindow) NXVariables.lw.WriteFullline(e.ToString());
                        return;
                    }
                    catch (Exception e)
                    {
                        if (UserVariables.useListingWindow) NXVariables.lw.WriteFullline(e.ToString());
                        return;
                    }
                    finally
                    {
                        ClosePart();
                    }

                    ResetNXVariables();
                }
            }

            string csvPath = UserVariables.dirPath + "FaceSearchSpace.csv";
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@csvPath, true))
            {
                file.WriteLine("Tolerance: " + UserVariables.boundingBoxTol.ToString());
                foreach (double searchSpace in NXVariables.avgFaceSearchSpace)
                {
                    file.WriteLine(searchSpace.ToString());
                }
            }

            if (UserVariables.useListingWindow)
            {
                NXVariables.lw.Close();
                NXVariables.lw.SelectDevice(ListingWindow.DeviceType.Window, "");
            }
        }

        public static void AdjacentFaces()
        {
            InitialiseNXVariables();

            string[] directories = Directory.GetDirectories(UserVariables.dirPath);

            foreach (string subDir in directories)
            {
                UserVariables.subDirPath = subDir;
                foreach (string inputFile in Directory.GetFiles(subDir, "*.x_t"))
                {
                    SetFilePaths(inputFile);

                    try
                    {
                        LoadNewPart(UserVariables.originalPartPath);
                        NXVariables.bodies = NXVariables.workPart.Bodies.ToArray();
                        LoadWorkFaces();

                        FileOperations.LoadPointCloud(UserVariables.unlabelledPCPath);
                        PointCloudOps.FindInternalFaces();
                        PointCloudOps.BoundingBoxOp();
                        PointCloudOps.FindAdjacentFace();

                        FileOperations.AddAdjFaceToCloudFile(UserVariables.oldLabelledPCPath, UserVariables.labelledPCPath);
                    }
                    catch (NXException e)
                    {
                        if (UserVariables.useListingWindow) NXVariables.lw.WriteFullline(e.ToString());
                        return;
                    }
                    catch (Exception e)
                    {
                        if (UserVariables.useListingWindow) NXVariables.lw.WriteFullline(e.ToString());
                        return;
                    }
                    finally
                    {
                        ClosePart();
                    }

                    ResetNXVariables();
                }
            }

            if (UserVariables.useListingWindow)
            {
                NXVariables.lw.Close();
                NXVariables.lw.SelectDevice(ListingWindow.DeviceType.Window, "");
            }
        }
    }
}
