﻿using System;
using System.Collections.Generic;

using NXOpen;
using NXOpen.UF;

namespace Point_Cloud_Creation_From_CAD_Models
{
    static class NXVariables
    {
        // Workspace properties
        public static Session s = Session.GetSession();
        public static UFSession ufs = UFSession.GetUFSession();
        public static UI ui = UI.GetUI();
        public static ListingWindow lw = s.ListingWindow;
        public static PartCleanup partCleanup;

        // Part properties
        public static Part workPart;
        public static Body[] bodies;
        public static Dictionary<Tag, WorkFace> workFaces = new Dictionary<Tag, WorkFace>();
        public static Dictionary<Tag, WorkPoint> workPoints = new Dictionary<Tag, WorkPoint>();

        // Tolerance Calculation
        public static List<double> avgDists = new List<double>();
        public static List<double> maxDists = new List<double>();
        public static List<int> faceSearchSpace = new List<int>();
        public static List<double> avgFaceSearchSpace = new List<double>();

        // Num of Point to Face Area Ratios
        public static List<double> ratios = new List<double>();
    }
}
