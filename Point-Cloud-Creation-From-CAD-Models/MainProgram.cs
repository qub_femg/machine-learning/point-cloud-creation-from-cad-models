﻿using System;

namespace Point_Cloud_Creation_From_CAD_Models
{
    public class MainProgram
    {
        public static void Main(string[] args)
        {
            // Change variables in method for your own needs
            SetInitUserVariables();

            Programs.ProcessDataset();

            //Programs.ToleranceCal();

            //Programs.SearchSpaceExperiment();

            //Programs.AdjacentFaces();
        }

        ///<summary>
        ///
        ///</summary>
        public static void SetInitUserVariables()
        {
            //Method types include: "basic", "new_points" or "relocation"
            UserVariables.methodType = "basic";
            UserVariables.fileExtension = "*.step";
            UserVariables.useListingWindow = true;
            UserVariables.saveWorkParts = false;
            UserVariables.isDecomposition = false;
            UserVariables.dirPath = "D://Mixed_Point_Clouds//Part_1//";
            UserVariables.cloudComparePath = @"C:\Program Files\CloudCompare";
            UserVariables.batchScriptName = "point_cloud_single.bat";
            UserVariables.pointCloudSize = 10000;

            //Default
            UserVariables.boundingBoxTol = 0.1;
            UserVariables.kdTolerance = 0.1;
            UserVariables.stlTriangleTol = 0.008;
            UserVariables.stlAdjacencyTol = 0.008;
            UserVariables.minNumPoints = 4;
        }

        public static int GetUnloadOption(string dummy)
        {
            return (int)NXOpen.Session.LibraryUnloadOption.Immediately;
        }
    }
}
