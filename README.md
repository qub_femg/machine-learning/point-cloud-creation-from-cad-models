# Point Cloud Creation From CAD Models
## About
CAD models are often represented as a **boundary representation B-Rep**. However, a machine learning algorithm requires a fixed input size across the entire dataset, which is not apparent for B-Rep models. Therefore, CAD models needed to be converted into a secondary representation such as **point clouds**.

This repo provides code to help with the creation and labeling of a point cloud dataset from CAD models. In short, the CAD model is discretized into a stl mesh, which is used to create a point cloud. This point cloud is superimposed onto the CAD model. Bounding boxes of each face in the CAD model are found. The points are linked to a face if it lies within the bounding box. 

![Process Flow](imgs/process_flow.jpg)


This method allows for the creation of point clouds for CAD models that have been decomposed for mesh generation.

The paper on which this code is based can be found [here](http://cad-journal.net/files/vol_18/CAD_18(4)_2021_760-771.pdf).

## Requirements
- **Language**: C#
- **CAD system**: Siemens NX <sup>(Proprietary)</sup>
- **Point cloud generator**: CloudCompare <sup>(Open-Source)</sup>
- **Visual Studio Framework**: Framework 4.6.1

## Setup
### Point cloud generator
Install CloudCompare from here. To run the software as a batch process, the Windows batch file must in added to your CloudCompare program file location.

### CAD System
To allow access to NX's API, the following .dll files need to be referenced in the Visual Studio project:
- NXOpen
- NXOpen.

These can be found in the NX Program File folder under / / /.
The KDTree dll file must also be added to the projects debug folder and referenced.

Once the project has been built. The project's dll can be added to NX. The easiest way of achieving this is by creating a command button to run the dll.

## Citation
Please cite this work if used in your research:

    @article{Point Cloud Dataset Creation for Machine Learning on CAD Models 2020,
      Author = {Andrew R. Colligan, Trevor. T. Robinson, Declan C. Nolan, Yang Hua},
      Journal = {Computer-Aided Design and Applications},
      Title = {Point Cloud Dataset Creation for Machine Learning on CAD Models},
      Year = {2020}
      URL = {http://cad-journal.net/files/vol_18/CAD_18(4)_2021_760-771.pdf}
    }

